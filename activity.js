const http = require("http");

const users = [
    {
        username: "jeff_chan17",
        password: "ballislife17"
    },
    {
        username: "d_miranda0",
        password: "mirandaPG",
    },
    {
        username: "arwind_spiderMan",
        password: "imspiderMan",
    },
];

http.createServer((req, res) => {
    console.log(`${new Date().toLocaleTimeString()} [${req.method}] - ${req.url}`);

    if (req.url === "/users" && req.method === "GET") { // [GET] /users
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(JSON.stringify(users));
    }
    else if (req.url === "/users" && req.method === "POST") { // [POST] /users
        let requestBody = "";
        req.on("data", (data) => requestBody += data);
        req.on("end", () => {
            try {
                requestBody = JSON.parse(requestBody);
            } catch (e) {
                res.writeHead(400, { "Content-Type": "application/json" });
                res.end(JSON.stringify({ error: "Could not parse json" }));
            }
            console.log("requestBody", requestBody);
            const { username, password } = requestBody;
            const newUser = { username, password };
            users.push(newUser);
            res.writeHead(201, { "Content-Type": "application/json" });
            res.end(JSON.stringify({ message: "Registration Successful" }));
        });
    }
    else if (req.url === "/users/login" && req.method === "POST") { // [POST] /users/login
        let requestBody = "";
        req.on("data", (data) => requestBody += data);
        req.on("end", () => {
            try {
                requestBody = JSON.parse(requestBody);
            } catch (e) {
                res.writeHead(400, { "Content-Type": "application/json" });
                res.end(JSON.stringify({ error: "Could not parse json" }));
            }
            console.log("requestBody", requestBody);
            const { username, password } = requestBody;
            const foundUser = users.find(
                (user) => user.username == username && user.password === password
            );
            console.log("foundUser", foundUser);
            if (foundUser) {
                res.writeHead(200, { "Content-Type": "application/json" });
                res.end(JSON.stringify(foundUser));
            } else {
                res.writeHead(401, { "Content-Type": "application/json" });
                res.end(JSON.stringify({ message: "Login failed. Wrong credentials" }));
            }
        });
    }
    else {
        res.writeHead(404, { "Content-Type": "text/plain" });
        res.end("Resource Not Found!");
    }
}).listen(8000);

console.log("Server running at localhost:8000");
